package com.elias.jenkins.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName HelloController
 * @Description TODO
 * @Author Elias
 * @Date 2021/4/15 21:42
 * @Version 1.0
 **/
@RestController
public class HelloController {

    @RequestMapping("hello")
    public String hello(){
        return "hello Jenkins";
    }
}
